import java.util.Scanner;

public class MaxSum {

  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
      int n = in.nextInt();
      int[] A = new int[n];
      int[] B = new int[n];

      for (int i = 0; i < n; ++i) {
        A[i] = in.nextInt();
      }

      for (int j = 0; j < n; ++j) {
        B[j] = in.nextInt();
      }

      int i0 = n - 1;
      int jMax = n - 1;
      int j0 = n - 1;

      for (int i = n - 1; i >= 0; --i) {
        if (B[i] >= B[jMax]) {
          jMax = i;
        }
        if (A[i] + B[jMax] >= A[i0] + B[j0]) {
          j0 = jMax;
          i0 = i;
        }
      }

      System.out.println(i0 + " " + j0); // copying like 11 characters is fine
    }
  }
}
