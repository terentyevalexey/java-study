import java.util.Scanner;

public class RequiredSum {

  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {

      int n = in.nextInt();
      int[] A = new int[n];
      for (int i = 0; i < n; ++i) {
        A[i] = in.nextInt();
      }

      int m = in.nextInt();
      int[] B = new int[m];
      for (int j = 0; j < m; ++j) {
        B[j] = in.nextInt();
      }

      int k = in.nextInt();

      int pairCount = 0;
      int i = 0;
      int j = m - 1;
      while (i < n && j >= 0) {
        if (A[i] + B[j] < k) {
          ++i;
        } else if (A[i] + B[j] > k) {
          --j;
        } else {
          ++pairCount;
          ++i;
          --j;
        }
      }
      System.out.println(pairCount);
    }
  }
}
