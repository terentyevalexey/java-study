import java.util.Scanner;

public class CountingOutGame {

  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
      int N = in.nextInt();
      int k = in.nextInt();
      System.out.println(countWinner(N, k));
    }
  }

  private static int countWinner(int N, int k) {
    if (N == 1) {
      return 1;
    }
    return 1 + (countWinner(N - 1, k) + k - 1) % N;
  }
}
