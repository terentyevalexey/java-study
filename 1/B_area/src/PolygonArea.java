import java.util.Scanner;

public class PolygonArea {

  public static void main(String[] args) {
    try (Scanner in = new Scanner(System.in)) {
      int n = in.nextInt();

      int firstX = in.nextInt();
      int firstY = in.nextInt();

      int previousX = firstX;
      int previousY = firstY;

      int doubledDirectedArea = 0;

      for (int i = 1; i < n; ++i) {
        int currentX = in.nextInt();
        int currentY = in.nextInt();
        doubledDirectedArea += previousX * currentY - currentX * previousY;
        previousX = currentX;
        previousY = currentY;
      }
      doubledDirectedArea += previousX * firstY - firstX * previousY;

      System.out.println(Math.abs(doubledDirectedArea) / 2.0);
    }
  }
}
