package com.github.terentyevalexey;

public class DataPackage {
  private final int destinationNode;

  private final String data;

  private final long startTime;

  DataPackage(int destinationNode, String data) {
    this.destinationNode = destinationNode;

    this.data = data;

    // Фиксируется время, когда создаётся пакет данных. Необходимо для
    // вычисления времени доставки до узла назначения.
    startTime = System.nanoTime();
  }


  /** get destination node of the package
   * @return destination node of the package
   */
  public int getDestinationNode() {
    return destinationNode;
  }

  /** get time of the package creation in nanoseconds
   * @return time of package creation
   */
  public long getStartTime() {
    return startTime;
  }

  /** get data that is inside of this package
   * @return data in package
   */
  public String getData() {
    return data;
  }
}
