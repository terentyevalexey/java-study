package com.github.terentyevalexey;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

/**
 * Node from token ring
 */
public class Node implements Runnable {

  private static final int COMPUTE_TIME = 1;
  static final int MAX_PACKAGES = 3;
  private final int nodeId;

  private final LongAdder bufferTime = new LongAdder();

  private final int corId;

  private final RingProcessor processor;

  private final LinkedBlockingQueue<DataPackage> buffer = new LinkedBlockingQueue<>();

  private ConcurrentLinkedQueue<DataPackage> allData;

  /**
   * constructs node from id, coordinator id and processor (needed to access list to write times, to
   * access next node and coordinator node, and to log)
   *
   * @param nodeId    current node id
   * @param corId     coordinator id
   * @param processor processor
   */
  Node(int nodeId, int corId, RingProcessor processor) {
    this.nodeId = nodeId;
    this.corId = corId;
    this.processor = processor;

    if (nodeId == corId) {
      allData = new ConcurrentLinkedQueue<>();
    }
  }

  /**
   * get id of the current node
   *
   * @return id of the current node
   */
  public int getId() {
    return nodeId;
  }

  /**
   * save data on coordinator
   *
   * @param data data to save
   */
  public void saveData(DataPackage data) {
    allData.add(data);
  }


  /** gets buffer of waiting packages
   * @return buffer of waiting packages
   */
  public LinkedBlockingQueue<DataPackage> getBuffer() {
    return buffer;
  }

  /**
   * get all data packages that were computed by token ring, method for coordinator
   *
   * @return all data packages computed
   */
  public ConcurrentLinkedQueue<DataPackage> getAllData() {
    return allData;
  }

  /**
   * set data to the buffer, wait if it's not available (doesn't wait long usually)
   *
   * @param data data to set
   * @throws InterruptedException if thread was interrupted
   */
  public void setData(DataPackage data) throws InterruptedException {
    buffer.put(data);
    bufferTime.add(-System.nanoTime());
  }

  /**
   * get data from the buffer, wait if it's not available yet
   *
   * @return taken data
   * @throws InterruptedException if thread was interrupted
   */
  public DataPackage getData() throws InterruptedException {
    DataPackage data = buffer.take();
    bufferTime.add(System.nanoTime());
    return data;
  }

  /**
   * get sum of times data stayed at buffer
   *
   * @return sum of times data stayed at buffer
   */
  public long getBufferTime() {
    return bufferTime.sum();
  }

  /**
   * Начало работы узла. То есть из Node.bufferStack берётся пакет с данными и отправляется на
   * обработку, после чего передаётся следующему узлу. Тут заключена логика, согласно которой
   * обрабатываться может только 3 пакета данных одновременно.
   */
  @Override
  public void run() {
    List<DataPackage> toCompute = new ArrayList<>();
    try {
      toCompute.add(getData());
    } catch (InterruptedException ignored) {
      // ignore, this probably means that we ended.
    }
    for (int i = 1; i < MAX_PACKAGES && !buffer.isEmpty(); ++i) {
      try {
        toCompute.add(getData());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    try {
      TimeUnit.MILLISECONDS.sleep(COMPUTE_TIME);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    for (DataPackage data : toCompute) {
      if (data.getDestinationNode() == getId()) {
        // no need to worry about order there, it doesn't matter because we only count average
        processor.getTimeList().add(System.nanoTime());
        processor.getNode(corId).saveData(data);
        processor.log("Package sent to coordinator from " + getId());
      } else {
        try {
          processor.getNext(nodeId).setData(data);
          processor.log(
              "Package sent from node " + getId() + " to next node " + processor.getNext(getId())
                  .getId());
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
