package com.github.terentyevalexey;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 * В конструкторе кольцо инициализируется, то есть создаются все узлы и данные на узлах. В методе
 * {@link RingProcessor#startProcessing()} запускается работа кольца - данные начинают
 * обрабатываться по часовой стрелке. Также производится логгирование в {@link RingProcessor#logs}.
 * Вся работа должна быть потокобезопасной и с обработкой всех возможных исключений. Если
 * необходимо, разрешается создавать собственные классы исключений.
 */
public class RingProcessor {

  private static final int WAIT_TIME = 1;

  private final int nodesAmount;
  private final int dataAmount;

  private static int coordinatorId;

  private final FileHandler logs;

  private List<Node> nodeList;

  private final Logger logger;

  /**
   * Сюда идёт запись времени прохода каждого пакета данных. Используется в {@link
   * RingProcessor#averageTime()} для подсчета среднего времени прохода данных к координатору.
   */

  private final List<Long> timeList = new ArrayList<>();

  /**
   * get end time list of processed packages
   *
   * @return time list
   */
  public List<Long> getTimeList() {
    return timeList;
  }

  public RingProcessor(int nodesAmount, int dataAmount, FileHandler logs)
      throws InterruptedException {
    this.nodesAmount = nodesAmount;

    this.dataAmount = dataAmount;

    this.logs = logs;

    logger = Logger.getLogger("ringLogger");
    if (logs != null) {
      logger.addHandler(this.logs);
    }
    init();
  }

  /**
   * gets node by id
   *
   * @param id node id
   * @return node on id position
   */
  Node getNode(int id) {
    return nodeList.get(id);
  }

  /**
   * get next node from id
   *
   * @param id current node id
   * @return next node on the ring
   */
  Node getNext(int id) {
    return nodeList.get((id + 1) % nodesAmount);
  }

  /**
   * log message (logs it as info)
   *
   * @param message message to log
   */
  void log(String message) {
    logger.info(message);
  }

  /**
   * calculates average time for package to reach coordinator
   *
   * @return average time to reach coordinator
   */
  public long averageTime() {
    return (long) (timeList.stream().mapToLong(i -> i).average().orElseThrow() - nodeList
        .get(coordinatorId).getAllData().stream().mapToLong(DataPackage::getStartTime).average()
        .orElseThrow());
  }

  /**
   * calculate average time of data stayed at buffer
   *
   * @return average time of data stayed at buffer
   */
  public long averageBufferTime() {
    return nodeList.stream().mapToLong(Node::getBufferTime).sum() / (dataAmount
        * nodesAmount);
  }

  private void init() throws InterruptedException {
    //initialize ring
    Random random = new Random();
    coordinatorId = random.nextInt(nodesAmount);
    nodeList = new ArrayList<>();
    ExecutorService executor = Executors.newCachedThreadPool();
    for (int nodeId = 0; nodeId < nodesAmount; ++nodeId) {
      Node createdNode = new Node(nodeId, coordinatorId, this);
      nodeList.add(createdNode);

      String data = random.nextInt() + " - from node " + nodeId + "\n";
      executor.submit(() -> {
        for (int dataNumber = 0; dataNumber < dataAmount; ++dataNumber) {
          DataPackage initialData = new DataPackage(random.nextInt(nodesAmount), data);
          try {
            createdNode.setData(initialData);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      });
    }
    executor.shutdown();
    executor.awaitTermination(WAIT_TIME, TimeUnit.SECONDS);
    log(String
        .format("Initialized token ring with %d nodes %d data packages and %d coordinator id.",
            nodesAmount, dataAmount, coordinatorId));
  }

  /**
   * implementation of the token ring task
   *
   * @throws InterruptedException if run was interrupted
   */
  public void startProcessing() throws InterruptedException {
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(nodesAmount);
    for (int i = 0; i < nodesAmount; ++i) {
      executor.scheduleWithFixedDelay(nodeList.get(i), 0, WAIT_TIME, TimeUnit.NANOSECONDS);
    }

    while (nodeList.get(coordinatorId).getAllData().size() != dataAmount * nodesAmount) {
      TimeUnit.MILLISECONDS.sleep(WAIT_TIME);
    }
    executor.shutdownNow();
    log("Average time of package to reach destination: " + averageTime());
    log("Average time of package to stay in buffer: " + averageBufferTime());
  }
}
