package com.github.terentyevalexey;

import java.io.IOException;
import java.util.logging.FileHandler;

public final class Main {

  private static final int NODES_AMOUNT = 3;
  private static final int DATA_AMOUNT = 1;

  private Main() {
  }

  public static void main(String[] args) throws IOException, InterruptedException {
    RingProcessor processor = new RingProcessor(NODES_AMOUNT, DATA_AMOUNT,
        new FileHandler("D://log.txt"));

    processor.startProcessing();
  }
}

