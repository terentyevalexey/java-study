package com.github.terentyevalexey;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

class NodeTest {

  @Test
  void testSaveData() {
    Node coordinatorNode = new Node(0, 0, null);
    DataPackage data = new DataPackage(1, "h");
    assertTimeoutPreemptively(Duration.ofSeconds(1), () -> coordinatorNode.saveData(data));
    assertTimeoutPreemptively(Duration.ofSeconds(1),
        () -> assertEquals(data, coordinatorNode.getAllData().poll()));
    assertTimeoutPreemptively(Duration.ofSeconds(1), () -> coordinatorNode.saveData(data));
    assertEquals(data, coordinatorNode.getAllData().poll());
    assertTrue(coordinatorNode.getAllData().isEmpty());
    Node node = new Node(0, 1, null);

    assertTimeoutPreemptively(Duration.ofSeconds(1),
        () -> assertThrows(NullPointerException.class, () -> node.saveData(data)));
  }

  @Test
  void testSetData() {
    Node node = new Node(0, 0, null);
    DataPackage data = new DataPackage(1, "h");
    assertTimeoutPreemptively(Duration.ofSeconds(1), () -> node.setData(data));
    assertTimeoutPreemptively(Duration.ofSeconds(1),
        () -> assertEquals(data, node.getBuffer().poll()));
    assertTimeoutPreemptively(Duration.ofSeconds(1),
        () -> assertTrue(node.getBuffer().isEmpty()));
  }

  @Test
  void testGetData() throws InterruptedException {
    Node node = new Node(0, 0, null);
    ExecutorService executor = Executors.newSingleThreadExecutor();
    executor.submit(node::getData);
    assertFalse(executor.awaitTermination(200, TimeUnit.MILLISECONDS));
    executor.shutdownNow();
  }

  @Test
  void testRun() throws InterruptedException {
    int nodesAmount = 10;
    int dataAmount = 3;
    RingProcessor processor = new RingProcessor(nodesAmount, dataAmount, null);
    assertEquals(dataAmount, processor.getNode(0).getBuffer().size());
    for (int i = 0; i < nodesAmount; ++i) {
      Node node = processor.getNode(i);
      Node nextNode = processor.getNext(i);
      long finished = node.getBuffer().stream().limit(Node.MAX_PACKAGES)
          .filter(data -> data.getDestinationNode() == node.getId()).count();
      assertTimeoutPreemptively(Duration.ofSeconds(1), node::run);
      if (i + 1 == nodesAmount) {
        finished += 3;
      }
      assertEquals(Node.MAX_PACKAGES + dataAmount - finished, nextNode.getBuffer().size());
    }
  }
}