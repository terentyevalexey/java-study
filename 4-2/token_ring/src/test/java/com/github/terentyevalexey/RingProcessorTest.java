package com.github.terentyevalexey;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import org.junit.jupiter.api.Test;

class RingProcessorTest {

  private RingProcessor processor;

  void setup(int nodesAmount, int dataAmount) throws InterruptedException {
    processor = new RingProcessor(nodesAmount, dataAmount, null);
  }

  @Test
  void testGetNode() throws InterruptedException {
    int nodesAmount = 10;
    setup(nodesAmount, 3);
    for (int i = 0; i < nodesAmount; ++i) {
      assertEquals(i, processor.getNode(i).getId());
    }
  }

  @Test
  void testGetNext() throws InterruptedException {
    int nodesAmount = 20;
    setup(nodesAmount, 5);
    for (int i = 0; i < nodesAmount; ++i) {
      assertEquals((i + 1) % nodesAmount, processor.getNext(i).getId());
    }
  }

  @Test
  void testStartProcessing() throws InterruptedException {
    setup(20, 10);
    assertTimeout(Duration.ofSeconds(5), processor::startProcessing);
  }

  @Test
  void testAverageTime() throws InterruptedException {
    for (int i = 2; i < 10; ++i) {
      for (int j = 1; j < 3; ++j) {
        setup(i, j);
        assertTimeout(Duration.ofSeconds(2), processor::startProcessing);
        assertTrue(processor.averageTime() < 1000000000);
      }
    }
  }

  @Test
  void testAverageBufferTime() throws InterruptedException {
    for (int i = 2; i < 30; ++i) {
      for (int j = 1; j < 5; ++j) {
        setup(i, j);
        assertTimeout(Duration.ofSeconds(10), processor::startProcessing);
        long averageBufferTime = processor.averageBufferTime();
        assertTrue(averageBufferTime > 0);
        assertTrue(averageBufferTime < processor.averageTime());
      }
    }
  }
}