package com.github.terentyevalexey;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Scanner;
import com.github.terentyevalexey.Piece.Color;
import com.github.terentyevalexey.checkers_exceptions.BusyCellException;
import com.github.terentyevalexey.checkers_exceptions.CheckersException;
import com.github.terentyevalexey.checkers_exceptions.InvalidMoveException;
import com.github.terentyevalexey.checkers_exceptions.WhiteCellException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RussianCheckersCalculatorTest {

  private RussianCheckersCalculator calculator;

  @BeforeEach
  void setUp() {
    calculator = new RussianCheckersCalculator();
  }

  @Test
  void testSetInitialPosition1() throws CheckersException {
    String position = "a1 a3 b2 c1 c3 d2 e1 e3 f2 g1 g3 h2 \n"
        + "a7 b6 b8 c7 d6 d8 e7 f6 f8 g7 h6 h8 ";
    calculator.setInitialPosition(new Scanner(position));
    assertEquals(position, calculator.toString());
  }

  @Test
  void testSetInitialPositionWhite() throws CheckersException {
    String position = "a1 a3 b2 c1 c3 d2 e1 e3 f2 g1 g3 h2 \n";
    calculator.setInitialPosition(new Scanner(position), Color.WHITE);
    assertEquals(position, calculator.toString());
  }

  @Test
  void testSetInitialPositionBlack() throws CheckersException {
    String position = "a1 a3 b2 c1 c3 d2 e1 e3 f2 g1 g3 h2 \n";
    String expected = "\na1 a3 b2 c1 c3 d2 e1 e3 f2 g1 g3 h2 ";
    calculator.setInitialPosition(new Scanner(position), Color.BLACK);
    assertEquals(expected, calculator.toString());
  }

  @Test
  void testCalculateMoveWrongNotation() throws CheckersException {
    String position = "a1 a3 b2 c1 c3 d2 e1 e3 f2 g1 g3 h2 \n"
        + "a7 b6 b8 c7 d6 d8 e7 f6 f8 g7 h6 h8 ";
    calculator.setInitialPosition(new Scanner(position));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("A3-A"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("3-B4"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("A3-B7-A8"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("A3:A5-A8"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("a3:"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("c3-"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("-A4"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner(":A6"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("A3"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("A3-A0"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("A3-A9"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("a3-a5"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("A3-B4"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("a3-b4"), Color.BLACK));
  }

  @Test
  void testCalculateMove() throws CheckersException {
    String position = "a1 a3 b2 c1 c3 d2 e1 e3 f2 g1 g3 h2 \n"
        + "a7 b6 b8 c7 d6 d8 e7 f6 f8 g7 h6 h8 ";
    calculator.setInitialPosition(new Scanner(position));
    calculator.calculateMove(new Scanner("a3-b4"), Color.WHITE);

    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("a3-b4"), Color.WHITE));
    String expected = "a1 b2 b4 c1 c3 d2 e1 e3 f2 g1 g3 h2 \n"
        + "a7 b6 b8 c7 d6 d8 e7 f6 f8 g7 h6 h8 ";
    assertEquals(expected, calculator.toString());

    assertFalse(calculator.calculateMove(new Scanner("c3-d4"), Color.WHITE));
    calculator.calculateMove(new Scanner("b6-c5"), Color.BLACK);
    calculator.calculateMove(new Scanner("d4:b6"), Color.WHITE);
    expected = "a1 b2 b4 b6 c1 d2 e1 e3 f2 g1 g3 h2 \n"
        + "a7 b8 c7 d6 d8 e7 f6 f8 g7 h6 h8 ";
    assertEquals(expected, calculator.toString());

    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("a7-c5"), Color.BLACK));

    calculator = new RussianCheckersCalculator();
    calculator.setInitialPosition(new Scanner(expected));
    assertThrows(InvalidMoveException.class,
        () -> calculator.calculateMove(new Scanner("a7:c5"), Color.BLACK));

    calculator = new RussianCheckersCalculator();
    calculator.setInitialPosition(new Scanner(expected));
    calculator.calculateMove(new Scanner("a7:c5:a3"), Color.BLACK);
    expected = "a1 b2 c1 d2 e1 e3 f2 g1 g3 h2 \n"
        + "a3 b8 c7 d6 d8 e7 f6 f8 g7 h6 h8 ";
    assertEquals(expected, calculator.toString());
  }

  @Test
  void testCalculatePosition() throws CheckersException {
    String position = "a1 a3 b2 c1 c3 d2 e1 e3 f2 g1 g3 h2\n"
        + "a7 b6 b8 c7 d6 d8 e7 f6 f8 g7 h6 h8\n"
        + "g3-f4 f6-e5\n"
        + "c3-d4 e5:c3\n"
        + "b2:d4 d6-c5\n"
        + "d2-c3 g7-f6\n"
        + "h2-g3 h8-g7\n"
        + "c1-b2 f6-g5\n"
        + "g3-h4 g7-f6\n"
        + "f4-e5 f8-g7\n";
    String expected = "a1 a3 b2 c3 d4 e1 e3 e5 f2 g1 h4 \n"
        + "a7 b6 b8 c5 c7 d8 e7 f6 g5 g7 h6 ";
    calculator.calculatePosition(new Scanner(position));
    assertEquals(expected, calculator.toString());
  }

  @Test
  void testGetPositionByCode() throws WhiteCellException {
    for (int row = 1; row <= RussianCheckersCalculator.BOARD_WIDTH; ++row) {
      for (int column = 0; column < RussianCheckersCalculator.BOARD_WIDTH; ++column) {
        String code = String.valueOf((char) ('a' + column)) + row;
        if ((row + column) % 2 == 1) {
          assertEquals(calculator.compressPosition(column, row),
              calculator.getPositionByCode(code));
        } else {
          assertThrows(WhiteCellException.class, () -> calculator.getPositionByCode(code));
        }
      }
    }
  }

  @Test
  void testSetPieceByCode() throws WhiteCellException, BusyCellException {
    for (int row = 1; row <= RussianCheckersCalculator.BOARD_WIDTH; ++row) {
      for (int column = 0; column < RussianCheckersCalculator.BOARD_WIDTH; ++column) {
        String code = String.valueOf((char) ('a' + column)) + row;
        if ((row + column) % 2 == 0) {
          assertThrows(WhiteCellException.class, () -> calculator.setPieceByCode(code, null));
        } else {
          calculator.setPieceByCode(code, null);
        }
      }
    }
    for (int row = 1; row <= RussianCheckersCalculator.BOARD_WIDTH; ++row) {
      for (int column = 0; column < RussianCheckersCalculator.BOARD_WIDTH; ++column) {
        String code = String.valueOf((char) ('A' + column)) + row;
        if ((row + column) % 2 == 0) {
          assertThrows(WhiteCellException.class, () -> calculator.setPieceByCode(code, null));
        } else {
          assertThrows(BusyCellException.class, () -> calculator.setPieceByCode(code, null));
        }
      }
    }
  }
}