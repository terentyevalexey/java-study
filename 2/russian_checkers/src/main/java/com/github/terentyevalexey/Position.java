package com.github.terentyevalexey;

import java.util.Objects;

public class Position {

  private int column;
  private int row;

  public Position() {
  }

  public Position(int column, int row) {
    this.setColumn(column);
    this.setRow(row);
  }

  /** Indicates if positions are equal. If columns and rows are the same they're equal
   * @param o other object
   * @return if thi object equals to other object
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Position position = (Position) o;
    return getColumn() == position.getColumn()
        && getRow() == position.getRow();
  }

  /** Generate default hashcode from fields
   * @return hashcode
   */
  @Override
  public int hashCode() {
    return Objects.hash(getColumn(), getRow());
  }

  /** Column getter
   * @return column
   */
  public int getColumn() {
    return column;
  }

  /** Column setter
   * @param column column
   */
  public void setColumn(int column) {
    this.column = column;
  }

  /** Row getter
   * @return row
   */
  public int getRow() {
    return row;
  }

  /** Row setter
   * @param row row
   */
  public void setRow(int row) {
    this.row = row;
  }
}
