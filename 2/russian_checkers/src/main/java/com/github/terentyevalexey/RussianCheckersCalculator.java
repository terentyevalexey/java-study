package com.github.terentyevalexey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import com.github.terentyevalexey.Piece.Color;
import com.github.terentyevalexey.Piece.PieceTitle;
import com.github.terentyevalexey.checkers_exceptions.BusyCellException;
import com.github.terentyevalexey.checkers_exceptions.CheckersException;
import com.github.terentyevalexey.checkers_exceptions.InvalidMoveException;
import com.github.terentyevalexey.checkers_exceptions.WhiteCellException;

public class RussianCheckersCalculator {

  public static final int BOARD_HEIGHT = 4; // because we can't use whites
  public static final int BOARD_WIDTH = 2 * BOARD_HEIGHT;
  public static final int PIECES_PER_SIDE = 3 * BOARD_HEIGHT;
  public static final char MAX_KING_LETTER = 'A' + BOARD_WIDTH;
  public static final char MAX_MAN_LETTER = 'a' + BOARD_WIDTH;
  private static final int CODE_LETTERS_COUNT = 2;
  private static final int DELIMITER_LETTERS_COUNT = 1;
  private static final int REGEX_FIRST_CODE = 1;
  private static final int REGEX_MOVE_CODE = 4;
  private static final int REGEX_CAPTURE_CODE = 6;
  private static final int REGEX_CAPTURE_SHIFT = 3;
  private static final int MAN_MOVE_SHIFT = 1;
  private static final int MAN_CAPTURE_SHIFT = 2;


  private static final Pattern POSITION_REGEX = Pattern.compile(
      String.format("([a-%s]|[A-%s])[1-%s]", MAX_MAN_LETTER, MAX_KING_LETTER, BOARD_WIDTH));
  private static final Pattern MOVE_REGEX = Pattern.compile(String.format(
      "(([a-%1$s]|[A-%2$s])[1-%3$s])(-(([a-%1$s]|[A-%2$s])[1-%3$s])|"
          + ":(([a-%1$s]|[A-%2$s])[1-%3$s])"
          + "(:(([a-%1$s]|[A-%2$s])[1-%3$s]))?".repeat(PIECES_PER_SIDE - 1) + ")", MAX_MAN_LETTER,
      MAX_KING_LETTER, BOARD_WIDTH));

  private final Piece[][] board = new Piece[BOARD_WIDTH][BOARD_HEIGHT];


  /**
   * Calculates checkers game by given notation
   *
   * @param input Line of space split coded positions of white pieces, line of space split coded
   *              positions of black pieces, followed by alternating turns coded with move "-"
   *              delimiter or capture ":" delimiter (chained)
   * @throws BusyCellException    Target square is busy
   * @throws WhiteCellException   Target square is white (only black squares are in play)
   * @throws InvalidMoveException Must capture more pieces
   * @throws CheckersException    Other errors in notation
   */
  public void calculatePosition(Scanner input)
      throws CheckersException {
    setInitialPosition(input);
    Color side = Color.WHITE;
    while (input.hasNext()) {
      calculateMove(input, side);
      side = (side == Color.WHITE) ? Color.BLACK : Color.WHITE;
    }
  }

  /** Shows checkers notation of the current position on the board
   * @return Line of space split white piece codes followed by line of space split black piece
   *         codes all sorted left to right, bottom to top, starting with kings
   */
  public String toString() {
    ArrayList<String> whitePieces = new ArrayList<>();
    ArrayList<String> blackPieces = new ArrayList<>();
    for (char i = 0; i < BOARD_WIDTH; ++i) {
      for (char j = 0; j < BOARD_HEIGHT; ++j) {
        Position piece = new Position(i, j);
        Piece active = getPiece(piece);
        if (active != null) {
          char letter = (char) (i + (active.getType() == PieceTitle.KING ? 'A' : 'a'));
          String code = String.valueOf(letter) + decompressRow(piece) + " ";
          if (active.getSide() == Color.WHITE) {
            whitePieces.add(code);
          } else {
            blackPieces.add(code);
          }
        }
      }
    }
    StringBuilder answer = new StringBuilder();
    whitePieces.stream().sorted().forEach(answer::append);
    answer.append("\n");
    blackPieces.stream().sorted().forEach(answer::append);
    return answer.toString();
  }

  /**
   * Sets initial position of the board
   *
   * @param input Should start with proper initial position for both sides: 2 lines of pieces codes
   *              split by space (up to {@value PIECES_PER_SIDE} per line)
   * @throws WhiteCellException One of the codes refers to white cell
   * @throws BusyCellException  One of the coded positions already has a piece
   * @throws CheckersException  Too many codes given
   */
  public void setInitialPosition(Scanner input)
      throws CheckersException {
    setInitialPosition(input, Color.WHITE);
    setInitialPosition(input, Color.BLACK);
  }

  /**
   * Sets initial position of one side on the board
   *
   * @param input Should start with proper initial position for one side: line of pieces codes split
   *              by space (up to {@value PIECES_PER_SIDE} per line)
   * @param side  Color of the pieces in input
   * @throws WhiteCellException One of the codes refers to white cell
   * @throws BusyCellException  One of the coded positions already has a piece
   * @throws CheckersException  Too many codes given
   */
  public void setInitialPosition(Scanner input, Color side)
      throws CheckersException {
    if (side == Color.WHITE) {
      input.useDelimiter(" +");
    }
    int piecesPlaced = 0;
    while (input.hasNext(POSITION_REGEX)) {
      MatchResult match = input.match();
      setPieceByCode(match.group(), side);
      ++piecesPlaced;
      input.next(POSITION_REGEX);
    }

    if (side == Color.WHITE) {
      input.skip(" +");
      input.useDelimiter("\\p{javaWhitespace}+");
      if (input.findWithinHorizon(POSITION_REGEX, CODE_LETTERS_COUNT) != null) {
        MatchResult match = input.match();
        setPieceByCode(match.group(), side);
        ++piecesPlaced;
      }
    }
    if (piecesPlaced > PIECES_PER_SIDE) {
      throw new CheckersException();
    }
  }

  /** Calculates the next move, checks if input has correct move
   * @param input Should start with correct move notation
   * @param side Side of the piece moving
   * @return if calculated move is capture
   * @throws BusyCellException    Target square is busy
   * @throws WhiteCellException   Target square is white (only black squares are in play)
   * @throws InvalidMoveException Must capture more pieces
   * @throws CheckersException    Other errors in notation
   */
  public boolean calculateMove(Scanner input, Color side)
      throws CheckersException {
    // there is something wrong in the input (i.e. wrong notation)
    if (!input.hasNext(MOVE_REGEX)) {
      throw new CheckersException();
    }
    MatchResult match = input.match();
    input.next();
    // getting the initial position of the piece to move by code and check if this piece exists
    Position curPiece = getPositionByCode(match.group(REGEX_FIRST_CODE));
    Piece active = getPiece(curPiece);
    if (active == null || getTypeByCode(match.group(REGEX_FIRST_CODE)) != active.getType()
        || active.getSide() != side) {
      throw new CheckersException();
    }
    // checking current move: different action if it's capture
    if (match.group(REGEX_CAPTURE_CODE) != null) {
      calculateCapture(match, curPiece);
      return true;
    } else {
      calculateAdvance(match, curPiece);
      return false;
    }
  }

  /**
   * Check if the move (not capture) is possible
   *
   * @param source      Position of the piece that needs to move
   * @param destination Desired position after move
   * @throws BusyCellException Destination position has a piece on it
   * @throws CheckersException Move doesn't follow rules: different diagonal; or man moves too far
   */
  public void checkAdvance(Position source, Position destination)
      throws CheckersException {
    checkMove(source, destination, null, getPiece(source));
  }

  /**
   * Checks if destination is reachable from source by capturing according to russian checkers
   * rules
   *
   * @param source         Move start position
   * @param destination    Move end position
   * @param capturedPieces Already captured pieces during this capturing chain (null means advance)
   * @param active         Active piece (that makes the move)
   * @return Captured piece position
   * @throws BusyCellException Destination position has a piece on it
   * @throws CheckersException Move doesn't follow rules: different diagonal; or man moves too far;
   *                           or attempt to capture 2+ pieces at once was made; or attempted to
   *                           jump over own piece; or couldn't capture
   */
  public Position checkMove(Position source, Position destination,
      HashSet<Position> capturedPieces, Piece active)
      throws CheckersException {
    // checking if next cell is busy
    if (getPiece(destination) != null) {
      throw new BusyCellException();
    }
    // calculating difference in coordinates to iterate later
    int columnDiff = destination.getColumn() - source.getColumn();
    int rowDiff = decompressRow(destination) - decompressRow(source);
    int columnDiffSign = columnDiff > 0 ? 1 : -1;
    int rowDiffSign = rowDiff > 0 ? 1 : -1;
    columnDiff = Math.abs(columnDiff);
    rowDiff = Math.abs(rowDiff);
    // should be same diagonal
    if (columnDiff != rowDiff) {
      throw new CheckersException();
    }
    // should move
    if (rowDiff == 0) {
      throw new BusyCellException();
    }
    // we mark simple moves with capturedPieces set to nulls
    int capturesLeft = (capturedPieces == null) ? 0 : 1;
    // man can only move 1 square or capture jumping over 1 square
    if (active.getType() == PieceTitle.MAN && columnDiff != capturesLeft + 1) {
      throw new CheckersException();
    }
    // iterating over the diagonal between current and moved. there should be capturesLeft pieces
    Position capturedPosition = null;
    for (int i = 1; i < columnDiff; ++i) {
      Position intermediatePiece = compressPosition(source.getColumn() + columnDiffSign * i,
          decompressRow(source) + rowDiffSign * i);
      Piece intermediate = getPiece(intermediatePiece);
      if (intermediate != null) {
        if (capturesLeft-- > 0 && !capturedPieces.contains(intermediatePiece)
            && intermediate.getSide() != active.getSide()) {
          capturedPosition = intermediatePiece;
        } else {
          throw new CheckersException();
        }
      }
    }
    // didn't capture, but expected to capture
    if (capturesLeft > 0) {
      throw new CheckersException();
    }

    return capturedPosition;
  }

  /**
   * Moves piece and promotes it if it reached back rank
   *
   * @param source      Position of the piece that moves
   * @param destination Position of the destination of the move
   */
  public void promotePiece(Position source, Position destination) {
    Piece active = getPiece(source);
    if (destination.getRow() == BOARD_HEIGHT - 1 && active.getSide() == Color.WHITE
        && destination.getColumn() % 2 == 1
        || destination.getRow() == 0 && active.getSide() == Color.BLACK
        && destination.getColumn() % 2 == 0) {
      setPiece(destination, PieceTitle.KING, active.getSide());
    } else {
      setPiece(destination, active.getType(), active.getSide());
    }
    removePiece(source);
  }

  /**
   * Checks if any piece of active side is able to move, detects invalid moves
   *
   * @param side          Position of the piece to calculate routes from
   * @param capturesCount Number of captures to make
   * @throws InvalidMoveException Can capture more pieces than expected
   */
  public void checkAnyCaptures(Color side, int capturesCount)
      throws InvalidMoveException {
    for (int column = 0; column < BOARD_WIDTH; ++column) {
      for (int row = 0; row < BOARD_HEIGHT; ++row) {
        Position piece = new Position(column, row);
        Piece active = getPiece(piece);
        if (active != null && getPiece(piece).getSide() == side) {
          checkCaptures(piece, capturesCount);
        }
      }
    }
  }

  /**
   * Calculates capture chains available from current position, checking if the move is valid
   *
   * @param curPiece      Position of the piece to calculate routes from
   * @param capturesCount Number of captures to make
   * @throws InvalidMoveException Can capture more pieces than expected
   */
  public void checkCaptures(Position curPiece, int capturesCount)
      throws InvalidMoveException {
    ArrayList<Position> routes = new ArrayList<>();
    routes.add(curPiece);
    ArrayList<HashSet<Position>> capturedPieces = new ArrayList<>();
    HashSet<Position> curCapturedPieces = new HashSet<>();
    capturedPieces.add(curCapturedPieces);
    Piece active = getPiece(curPiece);
    for (int curCapturesCount = 0; curCapturesCount < PIECES_PER_SIDE && !routes.isEmpty();
        ++curCapturesCount) {
      // found longer chain
      if (curCapturesCount > capturesCount) {
        throw new InvalidMoveException();
      }
      int curRoutesCount = routes.size();
      for (int curRouteIndex = 0; curRouteIndex < curRoutesCount; ++curRouteIndex) {
        curPiece = routes.get(curRouteIndex);
        curCapturedPieces = capturedPieces.get(curRouteIndex);
        routes.set(curRouteIndex, null);
        // checking same diagonal
        int leftShiftBoarder = curPiece.getColumn() > MAN_MOVE_SHIFT ? (active.getType()
            == PieceTitle.KING ? -curPiece.getColumn() : -MAN_CAPTURE_SHIFT) : MAN_CAPTURE_SHIFT;
        int rightShiftBoarder = curPiece.getColumn() < BOARD_WIDTH - MAN_CAPTURE_SHIFT
            ? (active.getType() == PieceTitle.KING ? BOARD_WIDTH - curPiece.getColumn() - 1
            : MAN_CAPTURE_SHIFT) : -MAN_CAPTURE_SHIFT;
        for (int columnShift = leftShiftBoarder; columnShift <= rightShiftBoarder; ++columnShift) {
          if (columnShift == -MAN_MOVE_SHIFT) {
            columnShift = MAN_MOVE_SHIFT;
            continue;
          }
          for (int rowShift : Arrays.asList(columnShift, -columnShift)) {
            Position movedPiece = compressPosition(curPiece.getColumn() + columnShift,
                decompressRow(curPiece) + rowShift);
            // if we're out of boarders continue
            if (movedPiece.getRow() >= BOARD_HEIGHT || movedPiece.getRow() < 0) {
              continue;
            }
            try {
              Position capturedPiece = checkMove(curPiece, movedPiece, curCapturedPieces,
                  active);

              HashSet<Position> newCapturedPieces = new HashSet<>(curCapturedPieces);
              curCapturedPieces.add(capturedPiece);
              if (routes.get(curRouteIndex) == null) {
                routes.set(curRouteIndex, movedPiece);
              } else {
                routes.add(movedPiece);
                capturedPieces.add(curCapturedPieces);
              }
              curCapturedPieces = newCapturedPieces;
            } catch (CheckersException ignored) {
              // the exception is sent if the move is impossible
            }
          }
        }
      }
      for (int i = 0; i < routes.size(); ++i) {
        if (routes.get(i) == null) {
          routes.remove(i);
          capturedPieces.remove(i);
          --i;
        }
      }
    }
  }

  /** Get piece from position on the compressed board
   * @param pos position on the compressed board
   * @return piece on position given
   */
  public Piece getPiece(Position pos) {
    return board[pos.getColumn()][pos.getRow()];
  }

  /** Constructs new piece on the position on the compressed board
   * @param pos position on the compressed board
   * @param type type of the piece to create
   * @param side side of the piece to create
   */
  public void setPiece(Position pos, PieceTitle type, Color side) {
    board[pos.getColumn()][pos.getRow()] = new Piece(type, side);
  }

  /** Remove piece from the compressed board
   * @param pos position on the compressed board
   */
  public void removePiece(Position pos) {
    board[pos.getColumn()][pos.getRow()] = null;
  }

  /** Get compressed position from the default position
   * @param column column of the piece on the decompressed board
   * @param row row of the piece on the decompressed board
   * @return position on compressed board
   */
  public Position compressPosition(int column, int row) {
    return new Position(column, compressRow(row));
  }

  /**
   * Get type of the Piece (Man or King) by its code
   *
   * @param code Coded piece
   * @return Type of the piece
   */
  public PieceTitle getTypeByCode(String code) {
    return code.charAt(0) >= 'a' ? PieceTitle.MAN : PieceTitle.KING;
  }

  /**
   * Sets piece on the board by code and side
   *
   * @param code Coded position of the piece. i.e. a3 for man or  F6 for king
   * @param side Color of the figure: black or white
   * @throws WhiteCellException Coded position refers to white cell
   * @throws BusyCellException  Coded position already has a piece
   */
  public void setPieceByCode(String code, Color side)
      throws WhiteCellException, BusyCellException {
    PieceTitle type = getTypeByCode(code);
    Position piece = getPositionByCode(code);
    Piece active = getPiece(piece);
    if (active != null) {
      throw new BusyCellException();
    }
    setPiece(piece, type, side);
  }

  /**
   * Get Position of the coded piece on the compressed board
   *
   * @param code Coded position of the piece. i.e. a3 for man or  F6 for king
   * @return Position of the figure on the compressed checkers board
   * @throws WhiteCellException Coded position refers to white cell
   */
  public Position getPositionByCode(String code)
      throws WhiteCellException {
    int column = code.charAt(0) - (code.charAt(0) >= 'a' ? 'a' : 'A');
    // next string is to be changed for 10+ line checkers, but it works for 8-
    int row = code.charAt(1) - '0';
    if ((column + row) % 2 == 0) {
      throw new WhiteCellException();
    }
    return compressPosition(column, row);
  }

  /**
   * Get the compressed value of the row
   *
   * @param row Row number on the decompressed board
   * @return Row number on the compressed board
   */
  private int compressRow(int row) {
    return (row - 1) / 2;
  }

  /**
   * Get the row of Position on the decompressed board
   *
   * @param piece Position on the compressed board
   * @return Row of the position of decompressed board
   */
  private int decompressRow(Position piece) {
    return piece.getRow() * 2 + 1 + (piece.getColumn() % 2);
  }

  /**
   * Calculates capture
   *
   * @param match    Matched capture regex
   * @param curPiece Piece to move
   * @throws WhiteCellException   Coded position refers to white cell
   * @throws BusyCellException    One of the coded positions has a piece on it
   * @throws InvalidMoveException Can capture more pieces than expected
   * @throws CheckersException    Move doesn't follow rules: different diagonal; or man moves too
   *                              far; or attempt to capture 2+ pieces at once was made; or
   *                              attempted to jump over own piece; or couldn't capture enough
   *                              pieces;
   */
  private void calculateCapture(MatchResult match, Position curPiece)
      throws CheckersException {
    int capturesCount =
        (match.end() - match.start()) / (CODE_LETTERS_COUNT + DELIMITER_LETTERS_COUNT);
    ArrayList<Position> expectedRoute = new ArrayList<>();
    for (int i = 0; i < capturesCount; ++i) {
      Position nextPiece = getPositionByCode(
          match.group(REGEX_CAPTURE_CODE + REGEX_CAPTURE_SHIFT * i));
      expectedRoute.add(nextPiece);
    }

    Piece active = getPiece(curPiece);
    HashSet<Position> capturedPieces = new HashSet<>();
    // checking if the capture chain is valid
    Position prevPiece = curPiece;
    for (Position nextPiece : expectedRoute) {
      capturedPieces.add(checkMove(prevPiece, nextPiece, capturedPieces, active));
      prevPiece = nextPiece;
    }
    // getting available capture chains
    checkAnyCaptures(active.getSide(), capturesCount);
    Position movedPiece = expectedRoute.get(expectedRoute.size() - 1);
    capturedPieces.forEach(this::removePiece);
    promotePiece(curPiece, movedPiece);
  }

  /**
   * Calculate move (not capture)
   *
   * @param match    Matched move regex
   * @param curPiece Piece to move
   * @throws WhiteCellException   Matched coded position refers to white cell
   * @throws BusyCellException    Destination position has a piece on it
   * @throws InvalidMoveException Capture available
   * @throws CheckersException    Move doesn't follow rules: different diagonal; or man moves too
   *                              far
   */
  private void calculateAdvance(MatchResult match, Position curPiece)
      throws CheckersException {
    Position movedPiece = getPositionByCode(match.group(REGEX_MOVE_CODE));
    // check if the given square is reachable by moving
    checkAdvance(curPiece, movedPiece);
    // checking if able to take (then next method throws invalid move exception)
    checkAnyCaptures(getPiece(curPiece).getSide(), 0);

    // check for promotion
    promotePiece(curPiece, movedPiece);
  }
}
