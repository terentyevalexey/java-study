package com.github.terentyevalexey;

import java.util.Objects;

public class Piece {

  /** Type getter
   * @return type
   */
  public PieceTitle getType() {
    return type;
  }

  /** Type setter
   * @param type type
   */
  public void setType(PieceTitle type) {
    this.type = type;
  }

  /** Side getter
   * @return side
   */
  public Color getSide() {
    return side;
  }

  /** Side setter
   * @param side side
   */
  public void setSide(Color side) {
    this.side = side;
  }

  public enum PieceTitle { MAN, KING }

  public enum Color { BLACK, WHITE }

  private PieceTitle type;
  private Color side;

  public Piece(PieceTitle type, Color side) {
    this.setType(type);
    this.setSide(side);
  }

  /** Indicates if pieces are equal. If types and sides are equal then they're equal
   * @param o other object
   * @return if this piece equals to other object
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Piece piece = (Piece) o;
    return getType() == piece.getType()
        && getSide() == piece.getSide();
  }

  /** Returns default hashcode from type and side
   * @return hashcode of this object
   */
  @Override
  public int hashCode() {
    return Objects.hash(getType(), getSide());
  }
}
