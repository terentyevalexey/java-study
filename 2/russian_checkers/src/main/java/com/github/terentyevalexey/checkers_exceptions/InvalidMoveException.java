package com.github.terentyevalexey.checkers_exceptions;

public class InvalidMoveException extends CheckersException {

  public InvalidMoveException() {
    super("invalid move");
  }

  public InvalidMoveException(String message) {
    super(message);
  }
}
