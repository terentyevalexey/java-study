package com.github.terentyevalexey;

import java.util.Scanner;
import com.github.terentyevalexey.checkers_exceptions.CheckersException;

public final class RussianCheckers {

  private RussianCheckers() {
  }

  public static void main(String[] args) {
    try (Scanner input = new Scanner(System.in)) {
      RussianCheckersCalculator calculator = new RussianCheckersCalculator();
      calculator.calculatePosition(input);
      System.out.println(calculator);
    } catch (CheckersException e) {
      System.out.println(e.getMessage());
    }
  }
}
