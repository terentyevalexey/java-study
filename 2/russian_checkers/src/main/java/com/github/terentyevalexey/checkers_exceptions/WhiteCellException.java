package com.github.terentyevalexey.checkers_exceptions;

public class WhiteCellException extends CheckersException {

  public WhiteCellException() {
    super("white cell");
  }

  public WhiteCellException(String message) {
    super(message);
  }
}
