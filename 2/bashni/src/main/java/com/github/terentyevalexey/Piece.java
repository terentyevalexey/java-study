package com.github.terentyevalexey;

public class Piece {

  /** Type getter
   * @return type
   */
  public PieceTitle getType() {
    return type;
  }

  /** Type setter
   * @param type type
   */
  public void setType(PieceTitle type) {
    this.type = type;
  }

  /** Side getter
   * @return side
   */
  public Color getSide() {
    return side;
  }

  /** Side setter
   * @param side side
   */
  public void setSide(Color side) {
    this.side = side;
  }

  public enum PieceTitle { MAN, KING }

  public enum Color { BLACK, WHITE }

  private PieceTitle type;
  private Color side;

  public Piece(PieceTitle type, Color side) {
    this.setType(type);
    this.setSide(side);
  }

}
