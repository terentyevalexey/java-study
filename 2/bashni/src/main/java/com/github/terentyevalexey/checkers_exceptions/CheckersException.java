package com.github.terentyevalexey.checkers_exceptions;

public class CheckersException extends Exception {

  public CheckersException() {
    super("error");
  }

  public CheckersException(String message) {
    super(message);
  }
}
