package com.github.terentyevalexey;

import java.util.Scanner;
import com.github.terentyevalexey.checkers_exceptions.CheckersException;

public final class Towers {

  private Towers() {
  }

  public static void main(String[] args) {
    try (Scanner input = new Scanner(System.in)) {
      TowersCalculator calculator = new TowersCalculator();
      calculator.calculatePosition(input);
      System.out.println(calculator);
    } catch (CheckersException e) {
      System.out.println(e.getMessage());
    }
  }
}
