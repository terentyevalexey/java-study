package com.github.terentyevalexey.checkers_exceptions;

public class BusyCellException extends CheckersException {

  public BusyCellException() {
    super("busy cell");
  }

  public BusyCellException(String message) {
    super(message);
  }
}
