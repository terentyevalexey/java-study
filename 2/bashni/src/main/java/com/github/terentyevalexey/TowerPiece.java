package com.github.terentyevalexey;

import java.util.ArrayDeque;
import com.github.terentyevalexey.Piece.Color;
import com.github.terentyevalexey.Piece.PieceTitle;

public class TowerPiece {

  /**
   * Type getter
   *
   * @return type of the top piece
   */
  public PieceTitle getType() {
    return pieces.getFirst().getType();
  }

  /**
   * promotes the top piece of the tower
   */
  public void promote() {
    pieces.getFirst().setType(PieceTitle.KING);
  }


  /** checks if tower is empty (doesn't have pieces)
   * @return if the tower consists of no pieces
   */
  public boolean isEmpty() {
    return pieces.isEmpty();
  }

  /**
   * Side getter
   *
   * @return side of the piece
   */
  public Color getSide() {
    return pieces.getFirst().getSide();
  }

  /**
   * adds captured piece to tower
   *
   * @param captured captured piece to add to tower
   */
  public void add(Piece captured) {
    pieces.add(captured);
  }

  /**
   * remove one element from tower
   *
   * @return removed element
   */
  public Piece pop() {
    return pieces.pop();
  }

  private final ArrayDeque<Piece> pieces;

  /**
   * Constructs tower from side and arraydeque of pieces
   *
   * @param pieces Pieces to create tower
   */
  public TowerPiece(ArrayDeque<Piece> pieces) {
    this.pieces = pieces;
  }

  /**
   * Prints the code of the current piece i.e. bwwww or WbbBw
   *
   * @return code of this piece
   */
  @Override
  public String toString() {
    StringBuilder code = new StringBuilder();
    for (Piece active : pieces) {
      boolean isBlack = active.getSide() == Color.BLACK;
      code.append(
          active.getType() == PieceTitle.KING ? (isBlack ? 'B' : 'W') : (isBlack ? 'b' : 'w'));
    }
    return code.toString();
  }
}
