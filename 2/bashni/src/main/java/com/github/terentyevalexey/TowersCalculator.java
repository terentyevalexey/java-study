package com.github.terentyevalexey;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import com.github.terentyevalexey.Piece.Color;
import com.github.terentyevalexey.Piece.PieceTitle;
import com.github.terentyevalexey.checkers_exceptions.BusyCellException;
import com.github.terentyevalexey.checkers_exceptions.CheckersException;
import com.github.terentyevalexey.checkers_exceptions.InvalidMoveException;
import com.github.terentyevalexey.checkers_exceptions.WhiteCellException;

public class TowersCalculator {

  public static final int BOARD_HEIGHT = 4; // because we can't use whites
  public static final int BOARD_WIDTH = 2 * BOARD_HEIGHT;
  public static final int PIECES_PER_SIDE = 3 * BOARD_HEIGHT;
  public static final char MAX_KING_LETTER = 'A' + BOARD_WIDTH;
  public static final char MAX_MAN_LETTER = 'a' + BOARD_WIDTH;
  private static final int TOWER_PIECES_SHIFT = 3;
  private static final int REGEX_FIRST_CODE = 1;
  private static final int REGEX_MOVE_CODE = 5;
  private static final int REGEX_CAPTURE_CODE = 8;
  private static final int REGEX_CAPTURE_SHIFT = 4;
  private static final int MAN_MOVE_SHIFT = 1;
  private static final int MAN_CAPTURE_SHIFT = 2;

  private static final String CODE_REGEX = String
      .format("(([a-%1$s]|[A-%2$s])[1-%3$s])_([wWbB]{1,%4$s})", MAX_MAN_LETTER,
          MAX_KING_LETTER, BOARD_WIDTH, PIECES_PER_SIDE);
  private static final Pattern POSITION_REGEX = Pattern.compile(CODE_REGEX);
  private static final Pattern MOVE_REGEX = Pattern.compile(
      CODE_REGEX + "(-" + CODE_REGEX + "|:" + CODE_REGEX + ("(:" + CODE_REGEX + ")?")
          .repeat(PIECES_PER_SIDE - 1) + ")");


  private final TowerPiece[][] board = new TowerPiece[BOARD_WIDTH][BOARD_HEIGHT];

  /**
   * Calculates checkers game by given notation
   *
   * @param input Line of space split coded positions of white pieces, line of space split coded
   *              positions of black pieces, followed by alternating turns coded with move "-"
   *              delimiter or capture ":" delimiter (chained)
   * @throws BusyCellException    Target square is busy
   * @throws WhiteCellException   Target square is white (only black squares are in play)
   * @throws InvalidMoveException Must capture more pieces
   * @throws CheckersException    Other errors in notation
   */
  public void calculatePosition(Scanner input)
      throws CheckersException {
    setInitialPosition(input);
    Color side = Color.WHITE;
    while (input.hasNext()) {
      calculateMove(input, side);
      side = (side == Color.WHITE) ? Color.BLACK : Color.WHITE;
    }
  }

  /**
   * Shows checkers notation of the current position on the board
   *
   * @return Line of space split white piece codes followed by line of space split black piece codes
   * all sorted left to right, bottom to top, starting with kings
   */
  public String toString() {
    ArrayList<String> whitePieces = new ArrayList<>();
    ArrayList<String> blackPieces = new ArrayList<>();
    for (char i = 0; i < BOARD_WIDTH; ++i) {
      for (char j = 0; j < BOARD_HEIGHT; ++j) {
        Position piece = new Position(i, j);
        TowerPiece active = getPiece(piece);
        if (active != null) {
          char letter = (char) (i + 'a');
          String code = String.valueOf(letter) + decompressRow(piece) + "_" + active + " ";
          if (active.getSide() == Color.WHITE) {
            whitePieces.add(code);
          } else {
            blackPieces.add(code);
          }
        }
      }
    }
    StringBuilder answer = new StringBuilder();
    whitePieces.stream().sorted().forEach(answer::append);
    answer.append("\n");
    blackPieces.stream().sorted().forEach(answer::append);
    return answer.toString();
  }

  /**
   * Sets initial position of the board
   *
   * @param input Should start with proper initial position for both sides: 2 lines of pieces codes
   *              split by space (up to {@value PIECES_PER_SIDE} per line)
   * @throws WhiteCellException One of the codes refers to white cell
   * @throws BusyCellException  One of the coded positions already has a piece
   * @throws CheckersException  Too many codes given
   */
  public void setInitialPosition(Scanner input)
      throws CheckersException {
    setInitialPosition(input, Color.WHITE);
    setInitialPosition(input, Color.BLACK);
  }

  /**
   * Sets initial position of one side on the board
   *
   * @param input Should start with proper initial position for one side: line of pieces codes split
   *              by space (up to {@value PIECES_PER_SIDE} per line)
   * @param side  Color of the pieces in input
   * @throws WhiteCellException One of the codes refers to white cell
   * @throws BusyCellException  One of the coded positions already has a piece
   * @throws CheckersException  Too many codes given
   */
  public void setInitialPosition(Scanner input, Color side)
      throws CheckersException {
    if (side == Color.WHITE) {
      input.useDelimiter(" +");
    }
    int piecesPlaced = 0;
    while (input.hasNext(POSITION_REGEX)) {
      MatchResult match = input.match();
      setPieceByCode(match.group(), side);
      ++piecesPlaced;
      input.next(POSITION_REGEX);
    }

    if (side == Color.WHITE) {
      input.skip(" +");
      input.useDelimiter(" *\r?\n");
      if (input.hasNext(POSITION_REGEX)) {
        MatchResult match = input.match();
        setPieceByCode(match.group(), side);
        ++piecesPlaced;
        input.next();
      }
    }
    input.useDelimiter("\\p{javaWhitespace}+");
    if (piecesPlaced > PIECES_PER_SIDE) {
      throw new CheckersException();
    }
  }

  /**
   * Calculates the next move, checks if input has correct move
   *
   * @param input Should start with correct move notation
   * @param side  Side of the piece moving
   * @return if calculated move is capture
   * @throws BusyCellException    Target square is busy
   * @throws WhiteCellException   Target square is white (only black squares are in play)
   * @throws InvalidMoveException Must capture more pieces
   * @throws CheckersException    Other errors in notation
   */
  public boolean calculateMove(Scanner input, Color side)
      throws CheckersException {
    // there is something wrong in the input (i.e. wrong notation)
    if (!input.hasNext(MOVE_REGEX)) {
      throw new CheckersException();
    }
    MatchResult match = input.match();
    input.next();
    // getting the initial position of the piece to move by code and check if this piece exists
    Position curPiece = getPositionByCode(match.group(REGEX_FIRST_CODE));
    TowerPiece active = getPiece(curPiece);
    if (active == null || getTypeByCode(match.group()) != active.getType()
        || active.getSide() != side) {
      System.out.println(toString());
      throw new CheckersException();
    }
    // checking current move: different action if it's capture
    if (match.group(REGEX_CAPTURE_CODE) != null) {
      calculateCapture(match, curPiece);
      return true;
    } else {
      calculateAdvance(match, curPiece);
      return false;
    }
  }

  /**
   * Check if the move (not capture) is possible
   *
   * @param source      Position of the piece that needs to move
   * @param destination Desired position after move
   * @throws BusyCellException Destination position has a piece on it
   * @throws CheckersException Move doesn't follow rules: different diagonal; or man moves too far
   */
  public void checkAdvance(Position source, Position destination)
      throws CheckersException {
    checkMove(source, destination, null, getPiece(source));
  }

  /**
   * Checks if destination is reachable from source by capturing according to russian checkers
   * rules
   *
   * @param source         Move start position
   * @param destination    Move end position
   * @param capturedPieces Already captured pieces during this capturing chain (null means advance)
   * @param active         Active piece (that makes the move)
   * @return Captured piece position
   * @throws BusyCellException Destination position has a piece on it
   * @throws CheckersException Move doesn't follow rules: different diagonal; or man moves too far;
   *                           or attempt to capture 2+ pieces at once was made; or attempted to
   *                           jump over own piece; or couldn't capture
   */
  public Position checkMove(Position source, Position destination,
      HashSet<Position> capturedPieces, TowerPiece active)
      throws CheckersException {
    // checking if next cell is busy
    if (getPiece(destination) != null) {
      throw new BusyCellException();
    }
    // calculating difference in coordinates to iterate later
    int columnDiff = destination.getColumn() - source.getColumn();
    int rowDiff = decompressRow(destination) - decompressRow(source);
    int columnDiffSign = columnDiff > 0 ? 1 : -1;
    int rowDiffSign = rowDiff > 0 ? 1 : -1;
    columnDiff = Math.abs(columnDiff);
    rowDiff = Math.abs(rowDiff);
    // should be same diagonal
    if (columnDiff != rowDiff) {
      throw new CheckersException();
    }
    // should move
    if (rowDiff == 0) {
      throw new BusyCellException();
    }
    // we mark simple moves with capturedPieces set to nulls
    int capturesLeft = (capturedPieces == null) ? 0 : 1;
    // man can only move 1 square or capture jumping over 1 square
    if (active.getType() == PieceTitle.MAN && columnDiff != capturesLeft + 1) {
      throw new CheckersException();
    }
    // iterating over the diagonal between current and moved. there should be capturesLeft pieces
    Position capturedPosition = null;
    for (int i = 1; i < columnDiff; ++i) {
      Position intermediatePiece = compressPosition(source.getColumn() + columnDiffSign * i,
          decompressRow(source) + rowDiffSign * i);
      TowerPiece intermediate = getPiece(intermediatePiece);
      if (intermediate != null) {
        if (capturesLeft-- > 0 && !capturedPieces.contains(intermediatePiece)
            && intermediate.getSide() != active.getSide()) {
          capturedPosition = intermediatePiece;
        } else {
          throw new CheckersException();
        }
      }
    }
    // didn't capture, but expected to capture
    if (capturesLeft > 0) {
      throw new CheckersException();
    }

    return capturedPosition;
  }

  /**
   * checks for promotion of piece if it reached back rank
   *
   * @param piece position to check if it's back rank
   * @param side  color of the piece
   * @return if position is in back rank
   */
  public boolean isPromotable(Position piece, Color side) {
    return piece.getRow() == BOARD_HEIGHT - 1 && side == Color.WHITE
        && piece.getColumn() % 2 == 1
        || piece.getRow() == 0 && side == Color.BLACK
        && piece.getColumn() % 2 == 0;
  }

  /**
   * Checks if any piece of active side is able to move, detects invalid moves
   *
   * @param side Position of the piece to calculate routes from
   * @throws InvalidMoveException Can capture more pieces than expected
   */
  public void checkCaptures(Color side)
      throws InvalidMoveException {
    for (int column = 0; column < BOARD_WIDTH; ++column) {
      for (int row = 0; row < BOARD_HEIGHT; ++row) {
        Position piece = new Position(column, row);
        TowerPiece active = getPiece(piece);
        if (active != null && active.getSide() == side && canCapture(piece)) {
          throw new InvalidMoveException();
        }
      }
    }
  }

  /**
   * Calculates capture chains available from current position, checking if the move is valid
   *
   * @param curPiece Position of the piece to calculate routes from
   * @return if capture is possible
   */
  public boolean canCapture(Position curPiece) {
    TowerPiece active = getPiece(curPiece);
    // checking same diagonal
    int leftShiftBoarder = curPiece.getColumn() > MAN_MOVE_SHIFT ? (active.getType()
        == PieceTitle.KING ? -curPiece.getColumn() : -MAN_CAPTURE_SHIFT) : MAN_CAPTURE_SHIFT;
    int rightShiftBoarder = curPiece.getColumn() < BOARD_WIDTH - MAN_CAPTURE_SHIFT
        ? (active.getType() == PieceTitle.KING ? BOARD_WIDTH - curPiece.getColumn() - 1
        : MAN_CAPTURE_SHIFT) : -MAN_CAPTURE_SHIFT;
    for (int columnShift = leftShiftBoarder; columnShift <= rightShiftBoarder; ++columnShift) {
      if (columnShift == -MAN_MOVE_SHIFT) {
        columnShift = MAN_MOVE_SHIFT;
        continue;
      }
      for (int rowShift : Arrays.asList(columnShift, -columnShift)) {
        Position movedPiece = compressPosition(curPiece.getColumn() + columnShift,
            decompressRow(curPiece) + rowShift);
        // if we're out of boarders continue
        if (movedPiece.getRow() >= BOARD_HEIGHT || movedPiece.getRow() < 0) {
          continue;
        }
        try {
          checkMove(curPiece, movedPiece, new HashSet<>(), active);
          return true;
        } catch (CheckersException ignored) {
          // the exception is sent if the move is impossible
        }
      }
    }
    return false;
  }


  /**
   * Get piece from position on the compressed board
   *
   * @param pos position on the compressed board
   * @return piece on position given
   */
  public TowerPiece getPiece(Position pos) {
    return board[pos.getColumn()][pos.getRow()];
  }

  /**
   * Puts piece on the position on the compressed board
   *
   * @param pos   position on the compressed board
   * @param tower tower to put on the board
   */
  public void setPiece(Position pos, TowerPiece tower) {
    board[pos.getColumn()][pos.getRow()] = tower;
  }

  /**
   * Remove tower from the compressed board
   *
   * @param pos position on the compressed board
   */
  public void removePiece(Position pos) {
    board[pos.getColumn()][pos.getRow()] = null;
  }

  /**
   * removes one piece from the tower
   *
   * @param pos position  of the tower on the compressed board
   */
  public Piece capturePiece(Position pos) {
    Piece popped = board[pos.getColumn()][pos.getRow()].pop();
    if (board[pos.getColumn()][pos.getRow()].isEmpty()) {
      board[pos.getColumn()][pos.getRow()] = null;
    }
    return popped;
  }

  /**
   * Get compressed position from the default position
   *
   * @param column column of the piece on the decompressed board
   * @param row    row of the piece on the decompressed board
   * @return position on compressed board
   */
  public Position compressPosition(int column, int row) {
    return new Position(column, compressRow(row));
  }

  /**
   * Get type of the Piece (Man or King) by its code
   *
   * @param code Coded piece
   * @return Type of the piece
   */
  public PieceTitle getTypeByCode(String code) {
    return code.charAt(TOWER_PIECES_SHIFT) >= 'a' ? PieceTitle.MAN : PieceTitle.KING;
  }

  /**
   * Sets piece on the board by code and side
   *
   * @param code Coded position of the piece. i.e. a3_bwwB for man or  F6_WBww for king
   * @param side Color of the figure: black or white
   * @throws WhiteCellException Coded position refers to white cell
   * @throws BusyCellException  Coded position already has a piece
   * @throws CheckersException  Coded position is meant to be for one side, but it has different
   *                            color piece on top
   */
  public void setPieceByCode(String code, Color side)
      throws CheckersException {
    Position piece = getPositionByCode(code);
    TowerPiece tower = getTowerByCode(code);
    if (tower.getSide() != side) {
      throw new CheckersException();
    }
    TowerPiece active = getPiece(piece);
    if (active != null) {
      throw new BusyCellException();
    }
    setPiece(piece, tower);
  }

  /**
   * construct tower from tower code
   *
   * @param code code to construct from
   * @return tower built from code given
   */
  public TowerPiece getTowerByCode(String code) {
    ArrayDeque<Piece> pieces = new ArrayDeque<>();
    for (int i = TOWER_PIECES_SHIFT; i < code.length(); ++i) {
      switch (code.charAt(i)) {
        case 'b':
          pieces.add(new Piece(PieceTitle.MAN, Color.BLACK));
          break;
        case 'B':
          pieces.add(new Piece(PieceTitle.KING, Color.BLACK));
          break;
        case 'w':
          pieces.add(new Piece(PieceTitle.MAN, Color.WHITE));
          break;
        case 'W':
          pieces.add(new Piece(PieceTitle.KING, Color.WHITE));
          break;
      }
    }
    return new TowerPiece(pieces);
  }

  /**
   * Get Position of the coded piece on the compressed board
   *
   * @param code Coded position of the piece. i.e. a3 for man or  F6 for king
   * @return Position of the figure on the compressed checkers board
   * @throws WhiteCellException Coded position refers to white cell
   */
  public Position getPositionByCode(String code)
      throws WhiteCellException {
    int column = code.charAt(0) - (code.charAt(0) >= 'a' ? 'a' : 'A');
    // next string is to be changed for 10+ line checkers, but it works for 8-
    int row = code.charAt(1) - '0';
    if ((column + row) % 2 == 0) {
      throw new WhiteCellException();
    }
    return compressPosition(column, row);
  }

  /**
   * Get the compressed value of the row
   *
   * @param row Row number on the decompressed board
   * @return Row number on the compressed board
   */
  private int compressRow(int row) {
    return (row - 1) / 2;
  }

  /**
   * Get the row of Position on the decompressed board
   *
   * @param piece Position on the compressed board
   * @return Row of the position of decompressed board
   */
  private int decompressRow(Position piece) {
    return piece.getRow() * 2 + 1 + (piece.getColumn() % 2);
  }

  /**
   * Calculates capture
   *
   * @param match    Matched capture regex
   * @param curPiece Piece to move
   * @throws WhiteCellException   Coded position refers to white cell
   * @throws BusyCellException    One of the coded positions has a piece on it
   * @throws InvalidMoveException Can capture more pieces than expected
   * @throws CheckersException    Move doesn't follow rules: different diagonal; or man moves too
   *                              far; or attempt to capture 2+ pieces at once was made; or
   *                              attempted to jump over own piece; or couldn't capture enough
   *                              pieces;
   */
  private void calculateCapture(MatchResult match, Position curPiece)
      throws CheckersException {
    ArrayList<Position> expectedRoute = new ArrayList<>();

    for (int i = 0; REGEX_CAPTURE_CODE + REGEX_CAPTURE_SHIFT * i <= match.groupCount(); ++i) {
      String code = match.group(REGEX_CAPTURE_CODE + REGEX_CAPTURE_SHIFT * i);
      if (code == null) {
        break;
      }
      Position nextPiece = getPositionByCode(code);
      expectedRoute.add(nextPiece);
    }

    TowerPiece active = getPiece(curPiece);
    HashSet<Position> capturedPieces = new HashSet<>();
    // checking if the capture chain is valid
    Position prevPiece = curPiece;
    boolean promoted = false;
    for (Position nextPiece : expectedRoute) {
      capturedPieces.add(checkMove(prevPiece, nextPiece, capturedPieces, active));
      if (isPromotable(nextPiece, active.getSide())) {
        promoted = true;
      }
      prevPiece = nextPiece;
    }
    // getting available capture chains
    Position movedPiece = expectedRoute.get(expectedRoute.size() - 1);
    capturedPieces.forEach(capturedTower -> active.add(capturePiece(capturedTower)));
    setPiece(movedPiece, active);
    removePiece(curPiece);
    if (promoted) {
      active.promote();
    }
  }

  /**
   * Calculate move (not capture)
   *
   * @param match    Matched move regex
   * @param curPiece Piece to move
   * @throws WhiteCellException   Matched coded position refers to white cell
   * @throws BusyCellException    Destination position has a piece on it
   * @throws InvalidMoveException Capture available
   * @throws CheckersException    Move doesn't follow rules: different diagonal; or man moves too
   *                              far
   */
  private void calculateAdvance(MatchResult match, Position curPiece)
      throws CheckersException {
    Position movedPiece = getPositionByCode(match.group(REGEX_MOVE_CODE));
    // check if the given square is reachable by moving
    checkAdvance(curPiece, movedPiece);
    // checking if able to take (then next method throws invalid move exception)
    checkCaptures(getPiece(curPiece).getSide());

    // check for promotion
    TowerPiece active = getPiece(curPiece);
    setPiece(movedPiece, active);
    removePiece(curPiece);
    if (isPromotable(movedPiece, active.getSide())) {
      active.promote();
    }
  }
}
