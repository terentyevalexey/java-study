package com.github.terentyevalexey;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Scanner;
import com.github.terentyevalexey.Piece.Color;
import com.github.terentyevalexey.TowersCalculator;
import com.github.terentyevalexey.checkers_exceptions.BusyCellException;
import com.github.terentyevalexey.checkers_exceptions.CheckersException;
import com.github.terentyevalexey.checkers_exceptions.InvalidMoveException;
import com.github.terentyevalexey.checkers_exceptions.WhiteCellException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TowersCalculatorTest {

  private TowersCalculator calculator;

  @BeforeEach
  void setUp() {
    calculator = new TowersCalculator();
  }

  @Test
  void testSetInitialPosition1() throws CheckersException {
    String position = "a7_wbb b2_ww c1_w e1_w f2_w g1_w \n"
        + "b4_bwww b8_b c3_b c7_b e5_bww e7_b f8_b g5_b g7_b h8_b ";
    calculator.setInitialPosition(new Scanner(position));
    assertEquals(position, calculator.toString());
  }

  @Test
  void testSetInitialPositionWhite() throws CheckersException {
    String position = "a7_wbb b2_ww c1_w e1_w f2_w g1_w \n";
    calculator.setInitialPosition(new Scanner(position), Color.WHITE);
    assertEquals(position, calculator.toString());
  }

  @Test
  void testSetInitialPositionBlack() throws CheckersException {
    String position = "b4_bwww b8_b c3_b c7_b e5_bww e7_b f8_b g5_b g7_b h8_b \n";
    String expected = "\nb4_bwww b8_b c3_b c7_b e5_bww e7_b f8_b g5_b g7_b h8_b ";
    calculator.setInitialPosition(new Scanner(position), Color.BLACK);
    assertEquals(expected, calculator.toString());
  }

  @Test
  void testCalculateMoveWrongNotation() throws CheckersException {
    String position = "a7_wbb b2_ww c1_w e1_w f2_w g1_w \n"
        + "b4_bwww b8_b c3_b c7_b e5_bww e7_b f8_b g5_b g7_b h8_b ";
    calculator.setInitialPosition(new Scanner(position));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("A3-A"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("3-B4"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("A3-B7"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("A3:A5"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("a3:"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("b2_ww:"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("b2_ww-"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner(":d4_wwb"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("-d4_wwb"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("b2_ww:d0_wwb"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("b2_ww:d9_wwb"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("b0_ww:d7_wwb"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("t2_ww:d9_wwb"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("b2_:d4_wwb"), null));
    assertThrows(CheckersException.class,
        () -> calculator.calculateMove(new Scanner("b2_ww:d4_"), null));

    assertThrows(InvalidMoveException.class,
        () -> calculator.calculateMove(new Scanner("f2_w-g3_w"), Color.WHITE));

  }

  @Test
  void testCalculateMove() throws CheckersException {
    String position = "a7_wbb b2_ww c1_w e1_w f2_w g1_w \n"
        + "b4_bwww b8_b c3_b c7_b e5_bww e7_b f8_b g5_b g7_b h8_b ";
    calculator.setInitialPosition(new Scanner(position));
    calculator.calculateMove(new Scanner("b2_ww:d4_wwb:f6_wwbb:d8_wwbbb:b6_wwbbbb"), Color.WHITE);
  }

  @Test
  void testCalculatePosition() throws CheckersException {
    String position = "a7_wbb b2_ww c1_w e1_w f2_w g1_w\n"
        + "b4_bwww b8_b c3_b c7_b e5_bww e7_b f8_b g5_b g7_b h8_b\n"
        + "b2_ww:d4_wwb:f6_wwbb:d8_wwbbb:b6_wwbbbb b4_bwww-a3_bwww\n";
    String expected = "a7_wbb b6_Wwbbbb c1_w e1_w e5_ww f2_w g1_w \n"
        + "a3_bwww b8_b f8_b g5_b g7_b h8_b ";
    calculator.calculatePosition(new Scanner(position));
    System.out.println(calculator.toString());
    assertEquals(expected, calculator.toString());
  }

  @Test
  void testGetPositionByCode() throws WhiteCellException {
    for (int row = 1; row <= TowersCalculator.BOARD_WIDTH; ++row) {
      for (int column = 0; column < TowersCalculator.BOARD_WIDTH; ++column) {
        String code = String.valueOf((char) ('a' + column)) + row;
        if ((row + column) % 2 == 1) {
          assertEquals(calculator.compressPosition(column, row),
              calculator.getPositionByCode(code));
        } else {
          assertThrows(WhiteCellException.class, () -> calculator.getPositionByCode(code));
        }
      }
    }
  }

  @Test
  void testSetPieceByCode() throws CheckersException {
    for (int row = 1; row <= TowersCalculator.BOARD_WIDTH; ++row) {
      for (int column = 0; column < TowersCalculator.BOARD_WIDTH; ++column) {
        String code = String.valueOf((char) ('a' + column)) + row + "_bbbwbbbbw";
        if ((row + column) % 2 == 0) {
          assertThrows(WhiteCellException.class, () -> calculator.setPieceByCode(code, Color.BLACK));
        } else {
          calculator.setPieceByCode(code, Color.BLACK);
        }
      }
    }
    for (int row = 1; row <= TowersCalculator.BOARD_WIDTH; ++row) {
      for (int column = 0; column < TowersCalculator.BOARD_WIDTH; ++column) {
        String code = String.valueOf((char) ('A' + column)) + row + "_bbbwbbbbw";
        if ((row + column) % 2 == 0) {
          assertThrows(WhiteCellException.class, () -> calculator.setPieceByCode(code, Color.BLACK));
        } else {
          assertThrows(BusyCellException.class, () -> calculator.setPieceByCode(code, Color.BLACK));
        }
      }
    }
  }
}