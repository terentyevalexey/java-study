package com.github.terentyevalexey;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import org.junit.jupiter.api.Test;

class HarborTest {

  @Test
  void testGetAddGet() {
    Harbor harbor = new Harbor();
    for (ShipSize shipSize : ShipSize.values()) {
      for (ProductType productType : ProductType.values()) {
        assertNull(harbor.getShip(productType));
        Ship ship = new Ship(productType, shipSize);
        harbor.addShip(ship);
        assertEquals(ship, harbor.getShip(productType));
        assertNull(harbor.getShip(productType));
      }
    }
  }

  @Test
  void testFIFO() {
    Harbor harbor = new Harbor();
    for (ShipSize shipSize : ShipSize.values()) {
      for (ProductType productType : ProductType.values()) {
        Ship ship1 = new Ship(productType, shipSize);
        Ship ship2 = new Ship(productType, shipSize);
        harbor.addShip(ship1);
        harbor.addShip(ship2);
        assertEquals(ship1, harbor.getShip(productType));
        assertEquals(ship2, harbor.getShip(productType));
        assertNull(harbor.getShip(productType));
      }
    }
  }

  @Test
  void testDifferentTypes() {
    Harbor harbor = new Harbor();
    List<ProductType> productTypes = Arrays.asList(ProductType.values());
    Stack<Ship> ships = new Stack<>();
    for (ShipSize shipSize : ShipSize.values()) {
      for (ProductType productType : productTypes) {
        Ship ship = new Ship(productType, shipSize);
        ships.add(ship);
        harbor.addShip(ship);
      }
      Collections.reverse(productTypes);
      for (ProductType productType : productTypes) {
        assertEquals(ships.pop(), harbor.getShip(productType));
        assertNull(harbor.getShip(productType));
      }
    }
  }
}