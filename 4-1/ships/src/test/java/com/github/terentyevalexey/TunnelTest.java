package com.github.terentyevalexey;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

class TunnelTest {

  @Test
  void testPassShip() throws InterruptedException {
    Harbor harbor = new Harbor();
    try (Tunnel tunnel = new Tunnel(harbor)) {

      for (ProductType productType : ProductType.values()) {
        for (ShipSize shipSize : ShipSize.values()) {
          Ship ship = new Ship(productType, shipSize);
          assertNull(harbor.getShip(productType));
          ExecutorService executor = Executors.newSingleThreadExecutor();
          executor.execute(() -> tunnel.passShip(ship));
          executor.awaitTermination(Ship.getTimeToPassTunnel() + 1, TimeUnit.SECONDS);
          assertEquals(ship, harbor.getShip(productType));
          assertNull(harbor.getShip(productType));
        }
      }
    }
  }
}