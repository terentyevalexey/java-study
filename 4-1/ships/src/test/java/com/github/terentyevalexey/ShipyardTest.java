package com.github.terentyevalexey;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

class ShipyardTest {

  @Test
  void testRun() throws InterruptedException {
    Harbor harbor = new Harbor();
    try (Tunnel tunnel = new Tunnel(harbor)) {
      Shipyard shipyard = new Shipyard(tunnel);
      ExecutorService executor = Executors.newSingleThreadExecutor();
      executor.submit(shipyard);
      executor.awaitTermination(Ship.getTimeToPassTunnel() + 1, TimeUnit.SECONDS);
      for (ProductType productType : ProductType.values()) {
        assertNotNull(harbor.getShip(productType));
        assertNull(harbor.getShip(productType));
      }
    }
  }
}