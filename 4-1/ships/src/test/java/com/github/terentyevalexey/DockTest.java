package com.github.terentyevalexey;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTimeout;

import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

class DockTest {

  @Test
  void testLoadShip() throws InterruptedException {
    Harbor harbor = new Harbor();
    ExecutorService executor = Executors
        .newFixedThreadPool(ProductType.values().length * ShipSize.values().length);
    int maxLoadTime = 0;
    for (ProductType productType : ProductType.values()) {
      Dock dock = new Dock(productType, harbor);
      for (ShipSize shipSize : ShipSize.values()) {
        Ship ship = new Ship(productType, shipSize);
        final int loadTime =
            dock.getPeriodDuration() * ship.getCapacity() / dock.getLoadingSpeed();
        maxLoadTime = Math.max(maxLoadTime, loadTime);

        executor.execute(() -> {
          assertTimeout(Duration.ofSeconds(loadTime + 1), () -> dock.loadShip(ship));
          assertEquals(ship.getCapacity() - ship.getCapacity() % dock.getLoadingSpeed(),
              ship.getShipmentSize());
        });
      }
    }
    executor.shutdown();
    executor.awaitTermination(maxLoadTime + 1, TimeUnit.SECONDS);
  }

  @Test
  void testRun() throws InterruptedException {
    final int capacitySum = Arrays.stream(ShipSize.values()).mapToInt(ShipSize::getCapacity).sum();
    Harbor harbor = new Harbor();
    ExecutorService executor = Executors
        .newFixedThreadPool(ProductType.values().length * ShipSize.values().length);
    int maxTimeout = 0;
    for (ProductType productType : ProductType.values()) {
      Dock dock = new Dock(productType, harbor);
      maxTimeout = Math
          .max(dock.getPeriodDuration() * capacitySum / dock.getLoadingSpeed(), maxTimeout);
      assertTimeout(Duration.ofSeconds(1), dock::run);
      for (ShipSize shipSize : ShipSize.values()) {
        harbor.addShip(new Ship(productType, shipSize));
        executor.submit(dock);
      }
    }
    executor.shutdown();
    executor.awaitTermination(maxTimeout + 1, TimeUnit.SECONDS);
    for (ProductType productType : ProductType.values()) {
      assertNull(harbor.getShip(productType));
    }
  }
}