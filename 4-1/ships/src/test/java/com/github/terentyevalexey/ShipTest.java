package com.github.terentyevalexey;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ShipTest {

  @Test
  void testShip() {
    for (ProductType productType : ProductType.values()) {
      for (ShipSize shipSize : ShipSize.values()) {
        Ship ship = new Ship(productType, shipSize);
        int capacity = ship.getCapacity();
        assertEquals(shipSize.getCapacity(), capacity);
        ship.fillShip(capacity / 2);
        assertEquals(capacity / 2, ship.getShipmentSize());
        ship.fillShip(capacity / 4);
        assertEquals(capacity / 2 + capacity / 4, ship.getShipmentSize());
        ship.fillShip(capacity);
        assertEquals(capacity, ship.getShipmentSize());
      }
    }
  }
}