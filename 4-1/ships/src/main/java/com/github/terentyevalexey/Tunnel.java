package com.github.terentyevalexey;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Tunnel that passes through to harbor ships created on shipyard. Remember to close every tunnel
 * you create.
 */
public class Tunnel implements AutoCloseable {

  private final int capacity;
  private final ExecutorService executor;
  private final Harbor harbor;

  public Tunnel(Harbor harbor) {
    this.capacity = 5;
    this.harbor = harbor;
    executor = Executors.newFixedThreadPool(capacity);
  }

  public Tunnel(Harbor harbor, int capacity) {
    this.capacity = capacity;
    this.harbor = harbor;
    executor = Executors.newFixedThreadPool(capacity);
  }

  /**
   * Passes ship through tunnel to the harbor
   *
   * @param ship ship to pass
   */
  public void passShip(Ship ship) {
    executor.submit(() -> {
      try {
        ship.passThroughTunnel();
        harbor.addShip(ship);
      } catch (InterruptedException ignore) {
        // ignore
      }
    });
  }

  /**
   * Shuts down {@link ExecutorService} used for parallel passing of ships.
   */
  @Override
  public void close() {
    executor.shutdownNow();
  }
}
