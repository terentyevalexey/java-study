package com.github.terentyevalexey;

/**
 * Type of products: Bread, bananas or clothes
 */
public enum ProductType { BREAD, BANANA, CLOTHES }
