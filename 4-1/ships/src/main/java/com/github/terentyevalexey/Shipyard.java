package com.github.terentyevalexey;

import java.util.Random;

/**
 * Shipyard that generates ships travelling to docks.
 */
public class Shipyard implements Runnable {

  private final Random random = new Random();
  private final Tunnel tunnel;

  public Shipyard(Tunnel tunnel) {
    this.tunnel = tunnel;
  }

  /**
   * Sends ship to the tunnel
   *
   * @param ship ship to send
   */
  private void sendToTunnel(Ship ship) {
    tunnel.passShip(ship);
  }

  /**
   * Generates ships to travel all the way to the docks
   */
  @Override
  public void run() {
    for (ProductType productType : ProductType.values()) {
      Ship ship = new Ship(productType, ShipSize.values()[random.nextInt(ShipSize.values().length)]);
      tunnel.passShip(ship);
    }
  }
}
