package com.github.terentyevalexey;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class ControlCenter {
  private static final int WORK_TIME = 100;
  private static final int DOCKS_COUNT = 15;
  private static final int GENERATOR_DELAY = 1;
  private static final int DELAY_TIME = 1;

  public void execute() {
    final Harbor harbor = new Harbor();
    try (Tunnel tunnel = new Tunnel(harbor)) {
      final Shipyard shipyard = new Shipyard(tunnel);
      final List<Dock> docks = new ArrayList<>();
      for (int i = 0; i < DOCKS_COUNT; ++i) {
        docks.add(new Dock(ProductType.values()[i % ProductType.values().length], harbor));
      }
      ScheduledExecutorService executor = Executors.newScheduledThreadPool(DOCKS_COUNT + 1);
      executor.scheduleWithFixedDelay(shipyard, 0, GENERATOR_DELAY, TimeUnit.SECONDS);
      for (Dock dock : docks) {
        executor.scheduleWithFixedDelay(dock, 0, DELAY_TIME, TimeUnit.MILLISECONDS);
      }
      try {
        TimeUnit.SECONDS.sleep(WORK_TIME);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      executor.shutdownNow();
    }
  }
}
