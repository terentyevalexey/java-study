package com.github.terentyevalexey;

import java.util.concurrent.TimeUnit;

/**
 * Dock that accepts ships from harbor and loads them
 */
public class Dock implements Runnable {

  private static final int DEFAULT_LOADING_SPEED = 10;
  private final int loadingSpeed;
  private final int periodDuration;
  private final ProductType productType;
  private final Harbor harbor;

  public Dock(ProductType productType, Harbor harbor) {
    this.productType = productType;
    this.harbor = harbor;
    this.loadingSpeed = DEFAULT_LOADING_SPEED;
    this.periodDuration = 1;
  }

  public Dock(ProductType productType, Harbor harbor, int loadingSpeed, int periodDuration) {
    this.productType = productType;
    this.harbor = harbor;
    this.loadingSpeed = loadingSpeed;
    this.periodDuration = periodDuration;
  }

  /** Gets amount of items loaded per period of loading
   * @return amount of items loaded per period of loading
   */
  public int getLoadingSpeed() {
    return loadingSpeed;
  }


  /** Gets time for one period of loading
   * @return time for one period of loading
   */
  public int getPeriodDuration() {
    return periodDuration;
  }

  /**
   * Loads ship with products
   *
   * @param ship ship to load
   * @throws InterruptedException if was interrupted during loading
   */
  public void loadShip(Ship ship) throws InterruptedException {
    int periods = (ship.getCapacity() - ship.getShipmentSize()) / loadingSpeed;
    if (periods > 0) {
      System.out.println("loading ship: " + System.nanoTime());
      TimeUnit.SECONDS.sleep(periodDuration * periods);
      System.out.println("loaded ship: " + System.nanoTime());
      ship.fillShip(loadingSpeed * periods);
    }
  }

  /**
   * Retrieves ship from the harbor if possible
   *
   * @return required ship, or null if it's not available
   */
  public Ship requestShip() {
    return harbor.getShip(productType);
  }

  /**
   * Loads ships from harbor and sends them away
   */
  @Override
  public void run() {
    Ship ship = requestShip();
    if (ship != null) {
      try {
        loadShip(ship);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
