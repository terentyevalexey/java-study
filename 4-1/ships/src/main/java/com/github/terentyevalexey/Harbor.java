package com.github.terentyevalexey;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Harbor that holds ships after they pass tunnel on their way to the docks.
 */
public class Harbor {

  private final List<ConcurrentLinkedQueue<Ship>> ships = new ArrayList<>();

  public Harbor() {
    for (int i = 0; i < ProductType.values().length; ++i) {
      ships.add(new ConcurrentLinkedQueue<>());
    }
  }

  /**
   * Retrieves ship from the harbor if possible
   *
   * @param productType shipment type
   * @return required ship, or null if it's not available
   */
  public Ship getShip(ProductType productType) {
    return ships.get(productType.ordinal()).poll();
  }

  /**
   * Adds ship to the harbor
   *
   * @param ship ship to add to the harbor
   */
  public void addShip(Ship ship) {
    ships.get(ship.getType().ordinal()).add(ship);
  }
}
