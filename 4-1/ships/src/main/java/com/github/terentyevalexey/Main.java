package com.github.terentyevalexey;


public final class Main {


  private Main() {
  }

  public static void main(String[] args) {
    ControlCenter controlCenter = new ControlCenter();
    controlCenter.execute();
  }
}

