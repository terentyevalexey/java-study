package com.github.terentyevalexey;

/**
 * Size of the boat: small with capacity of 10, medium with capacity of 50 and large with capacity
 * of 100
 */
public enum ShipSize {
  SMALL(10), MEDIUM(50), LARGE(100);
  private final int capacity;

  ShipSize(int capacity) {
    this.capacity = capacity;
  }

  public int getCapacity() {
    return capacity;
  }
}
