package com.github.terentyevalexey;

import java.util.concurrent.TimeUnit;

/**
 * Ship that travels from shipyard all the way to the dock
 */
public class Ship {

  private static final int TIME_TO_PASS_TUNNEL = 1;

  private final ProductType productType;
  private final ShipSize shipSize;
  private int shipmentSize = 0;

  public Ship(ProductType productType, ShipSize shipSize) {
    this.productType = productType;
    this.shipSize = shipSize;
  }

  public Ship(ProductType productType, ShipSize shipSize, int timeToPassTunnel) {
    this.productType = productType;
    this.shipSize = shipSize;
  }

  /**
   * Fills ship with required amount of products
   *
   * @param units required amount of products to fill
   */
  public void fillShip(int units) {
    shipmentSize += units;
    if (shipmentSize > getCapacity()) {
      shipmentSize = getCapacity();
    }
  }

  /** Gets time to pass tunnel in seconds
   * @return time to pass tunnel
   */
  public static int getTimeToPassTunnel() {
    return TIME_TO_PASS_TUNNEL;
  }

  /**
   * Gets type of products this ship carries
   *
   * @return type of products carried
   */
  public ProductType getType() {
    return productType;
  }

  /**
   * Gets current ship full capacity
   *
   * @return ship capacity
   */
  public int getCapacity() {
    return shipSize.getCapacity();
  }

  /**
   * Gets number of goods held currently by this ship
   *
   * @return number of items held
   */
  public int getShipmentSize() {
    return shipmentSize;
  }

  /**
   * Simulates passing through tunnel
   *
   * @throws InterruptedException if was interrupted in tunnel
   */
  public void passThroughTunnel() throws InterruptedException {
    System.out.println("passing tunnel: " + System.nanoTime());
    TimeUnit.SECONDS.sleep(getTimeToPassTunnel());
    System.out.println("passed tunnel: " + System.nanoTime());
  }
}
